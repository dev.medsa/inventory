<?php


namespace App\Repositories\Brand;


interface IBrandInterface
{
    public function all();
    public function find( int $brand_id);
    public function delete( int $brand_id);
    public function update( int $brand_id, array $brand_data);
    public function create( array $brand_data);
}