<?php


namespace App\Repositories\Location;

use App\Models\Location;

class LocationRepository implements ILocationInterface
{
    protected  $location;

    public function __construct(Location $location)
    {
        $this->location = $location;
    }

    public function all()
    {
        return $this->location::all();
    }

    public function find(int $location_id)
    {
        return $this->location::find($location_id);
    }

    public function delete(int $location_id)
    {
        $this->location::destroy($location_id);
    }

    public function update(int $location_id, array $location_data)
    {
        $find = $this->location::find($location_id);
        return $find->update($location_data);
    }

    public function create(array $location_data)
    {
        $this->location::create($location_data);
    }

    public function __call($method, $args)
    {
        return call_location_func_array([$this->location, $method], $args);
    }
}