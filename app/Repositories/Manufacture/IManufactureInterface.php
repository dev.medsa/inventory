<?php


namespace App\Repositories\Manufacture;


interface IManufactureInterface
{
    public function all();
    public function find( int $manufacture_id);
    public function delete( int $manufacture_id);
    public function update( int $manufacture_id, array $manufacture_data);
    public function create( array $manufacture_data);
}