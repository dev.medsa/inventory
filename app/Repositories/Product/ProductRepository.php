<?php


namespace App\Repositories\Product;

use App\Models\Product;

class ProductRepository implements IProductInterface
{
    protected  $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function all()
    {
        return $this->product::all();
    }

    public function find(int $product_id)
    {
        return $this->product::find($product_id);
    }

    public function delete(int $product_id)
    {
        $this->product::destroy($product_id);
    }

    public function update(int $product_id, array $product_data)
    {
        $find = $this->product::find($product_id);
        return $find->update($product_data);
    }

    public function create(array $product_data)
    {
        $this->product::create($product_data);
    }

    public function __call($method, $args)
    {
        return call_product_func_array([$this->product, $method], $args);
    }
}