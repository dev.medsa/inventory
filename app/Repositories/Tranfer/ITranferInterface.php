<?php


namespace App\Repositories\Tranfer;


interface ITranferInterface
{
    public function all();
    public function find( int $tranfer_id);
    public function delete( int $tranfer_id);
    public function update( int $tranfer_id, array $tranfer_data);
    public function create( array $tranfer_data);
}