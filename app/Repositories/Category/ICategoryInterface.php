<?php


namespace App\Repositories\Category;


interface ICategoryInterface
{
    public function all();
    public function find( int $category_id);
    public function delete( int $category_id);
    public function update( int $category_id, array $category_data);
    public function create( array $category_data);
}