import Vue from 'vue';
require('./bootstrap');
import router from'./Services/Router/Router';
new Vue({
    el: '#app',
    router: router
});
export default app;