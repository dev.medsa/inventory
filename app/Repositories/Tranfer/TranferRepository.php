<?php


namespace App\Repositories\Tranfer;

use App\Models\Tranfer;

class TranferRepository implements ITranferInterface
{
    protected  $tranfer;

    public function __construct(Tranfer $tranfer)
    {
        $this->tranfer = $tranfer;
    }

    public function all()
    {
        return $this->tranfer::all();
    }

    public function find(int $tranfer_id)
    {
        return $this->tranfer::find($tranfer_id);
    }

    public function delete(int $tranfer_id)
    {
        $this->tranfer::destroy($tranfer_id);
    }

    public function update(int $tranfer_id, array $tranfer_data)
    {
        $find = $this->tranfer::find($tranfer_id);
        return $find->update($tranfer_data);
    }

    public function create(array $tranfer_data)
    {
        $this->tranfer::create($tranfer_data);
    }

    public function __call($method, $args)
    {
        return call_tranfer_func_array([$this->tranfer, $method], $args);
    }
}