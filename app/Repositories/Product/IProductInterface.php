<?php


namespace App\Repositories\Product;


interface IProductInterface
{
    public function all();
    public function find( int $product_id);
    public function delete( int $product_id);
    public function update( int $product_id, array $product_data);
    public function create( array $product_data);
}