<?php


namespace App\Repositories\Manufacture;

use App\Models\Manufacture;

class ManufactureRepository implements IManufactureInterface
{
    protected  $manufacture;

    public function __construct(Manufacture $manufacture)
    {
        $this->manufacture = $manufacture;
    }

    public function all()
    {
        return $this->manufacture::all();
    }

    public function find(int $manufacture_id)
    {
        return $this->manufacture::find($manufacture_id);
    }

    public function delete(int $manufacture_id)
    {
        $this->manufacture::destroy($manufacture_id);
    }

    public function update(int $manufacture_id, array $manufacture_data)
    {
        $find = $this->manufacture::find($manufacture_id);
        return $find->update($manufacture_data);
    }

    public function create(array $manufacture_data)
    {
        $this->manufacture::create($manufacture_data);
    }

    public function __call($method, $args)
    {
        return call_manufacture_func_array([$this->manufacture, $method], $args);
    }
}