<?php

namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Brand\IBrandInterface',
            'App\Repositories\Brand\BrandRepository'
        );
        $this->app->bind(
            'App\Repositories\Category\ICategoryInterface',
            'App\Repositories\Category\CategoryRepository'
        );
        $this->app->bind(
            'App\Repositories\Description\IDescriptionInterface',
            'App\Repositories\Description\DescriptionRepository'
        );
        $this->app->bind(
            'App\Repositories\Location\ILocationInterface',
            'App\Repositories\Location\LocationRepository'
        );
        $this->app->bind(
            'App\Repositories\Manufacture\IManufactureInterface',
            'App\Repositories\Manufacture\ManufactureRepository'
        );
        $this->app->bind(
            'App\Repositories\Product\IProductInterface',
            'App\Repositories\Product\ProductRepository'
        );
        $this->app->bind(
            'App\Repositories\Tranfer\ITranferInterface',
            'App\Repositories\Tranfer\TranferRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
