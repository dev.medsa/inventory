<?php


namespace App\Repositories\Brand;

use App\Models\Brand;

class BrandRepository implements IBrandInterface
{
    protected  $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function all()
    {
        return $this->brand::all();
    }

    public function find(int $brand_id)
    {
        return $this->brand::find($brand_id);
    }

    public function orderBy( $param1 , string $param2)
    {
        return $this->brand::orderBy($param1, $param2);
    }

    public function delete(int $brand_id)
    {
        $this->brand::destroy($brand_id);
    }

    public function update(int $brand_id, array $brand_data)
    {
        $find = $this->brand::find($brand_id);
        return $find->update($brand_data);
    }

    public function create(array $brand_data)
    {
        $this->brand::create($brand_data);
    }

    public function __call($method, $args)
    {
        return call_brand_func_array([$this->brand, $method], $args);
    }
}