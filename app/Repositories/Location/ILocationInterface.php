<?php


namespace App\Repositories\Location;


interface ILocationInterface
{
    public function all();
    public function find( int $location_id);
    public function delete( int $location_id);
    public function update( int $location_id, array $location_data);
    public function create( array $location_data);
}