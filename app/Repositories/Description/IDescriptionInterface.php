<?php


namespace App\Repositories\Description;


interface IDescriptionInterface
{
    public function all();
    public function find( int $description_id);
    public function delete( int $description_id);
    public function update( int $description_id, array $description_data);
    public function create( array $description_data);
}