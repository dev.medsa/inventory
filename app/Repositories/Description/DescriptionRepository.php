<?php


namespace App\Repositories\Description;

use App\Models\Description;

class DescriptionRepository implements IDescriptionInterface
{
    protected  $description;

    public function __construct(Description $description)
    {
        $this->description = $description;
    }

    public function all()
    {
        return $this->description::all();
    }

    public function find(int $description_id)
    {
        return $this->description::find($description_id);
    }

    public function delete(int $description_id)
    {
        $this->description::destroy($description_id);
    }

    public function update(int $description_id, array $description_data)
    {
        $find = $this->description::find($description_id);
        return $find->update($description_data);
    }

    public function create(array $description_data)
    {
        $this->description::create($description_data);
    }

    public function __call($method, $args)
    {
        return call_description_func_array([$this->description, $method], $args);
    }
}