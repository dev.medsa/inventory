<?php


namespace App\Repositories\Category;

use App\Models\Category;

class CategoryRepository implements ICategoryInterface
{
    protected  $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function all()
    {
        return $this->category::all();
    }

    public function find(int $category_id)
    {
        return $this->category::find($category_id);
    }

    public function delete(int $category_id)
    {
        $this->category::destroy($category_id);
    }

    public function update(int $category_id, array $category_data)
    {
        $find = $this->category::find($category_id);
        return $find->update($category_data);
    }

    public function create(array $category_data)
    {
        $this->category::create($category_data);
    }

    public function __call($method, $args)
    {
        return call_category_func_array([$this->category, $method], $args);
    }
}