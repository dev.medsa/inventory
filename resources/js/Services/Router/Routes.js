import App from '../../components/App.vue';
import Brand from '../../components/Brands/index.vue';
import Categories from '../../components/Categories/index.vue';
import Descriptions from '../../components/Categories/index.vue';
import Locations from '../../components/Locations/index.vue';
import Manufacures from '../../components/Manufacures/index.vue';
import Products from '../../components/Products/index.vue';
const allUrl =[
    { path:'/',name:'App',component:App },
    { path: '/brand',name:'Brand',component: Brand },
    { path: '/categories',name:'Categories',component: Categories },
    { path: '/descriptions',name:'Descriptions', component: Descriptions },
    { path: '/locations',name:'Locations',component: Locations },
    { path: '/manufacures',name:'Manufacures',component: Manufacures },
    { path: '/products',name:'Products',component: Products }
];
export default allUrl;